package kozuchowski.com.toDoApp.controller;

import jakarta.validation.Valid;
import kozuchowski.com.toDoApp.dto.CreateUserDto;
import kozuchowski.com.toDoApp.exception.ObjectAlreadyExistException;
import kozuchowski.com.toDoApp.exception.PasswordMismatchException;
import kozuchowski.com.toDoApp.model.TodoUser;
import kozuchowski.com.toDoApp.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserServiceImpl userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("")
    public TodoUser create(@Valid @RequestBody CreateUserDto dto) throws ObjectAlreadyExistException, PasswordMismatchException {
        return userService.create(dto);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class, ObjectAlreadyExistException.class, PasswordMismatchException.class})
    public ResponseEntity<Map<String, String>> handleExceptions(Exception ex) {
        Map<String, String> errors = new HashMap<>();

        if (ex instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException validationException = (MethodArgumentNotValidException) ex;
            validationException.getBindingResult().getAllErrors().forEach(error -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                errors.put(fieldName, errorMessage);
            });
        }
        if (ex instanceof ObjectAlreadyExistException) {
            String errorMessage = ex.getMessage();
            errors.put("error", errorMessage);
        }

        if (ex instanceof PasswordMismatchException) {
            String errorMessage = ex.getMessage();
            errors.put("error", errorMessage);
        }

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
