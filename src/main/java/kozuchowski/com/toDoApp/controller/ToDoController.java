package kozuchowski.com.toDoApp.controller;

import jakarta.validation.Valid;
import kozuchowski.com.toDoApp.dto.CreateToDoDto;
import kozuchowski.com.toDoApp.service.ToDoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/todo")
public class ToDoController {

    private final ToDoServiceImpl toDoService;

    @Autowired
    public ToDoController(ToDoServiceImpl toDoService) {
        this.toDoService = toDoService;
    }

    @PostMapping("")
    public void addTodo(@RequestBody CreateToDoDto dto) {
        toDoService.create(dto);
    }
}
