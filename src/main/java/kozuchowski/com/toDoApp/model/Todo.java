package kozuchowski.com.toDoApp.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    private String title;
    private LocalDate deadline;
    private LocalDate createdOn;
    private LocalDate modifiedOn;
    private String status;

    public Todo(String title, LocalDate deadline) {
        this.title = title;
        this.deadline = deadline;
        this.createdOn = LocalDate.now();
        this.status = "Todo";
    }

}
