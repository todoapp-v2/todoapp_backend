package kozuchowski.com.toDoApp.service;

import kozuchowski.com.toDoApp.dto.CreateToDoDto;
import kozuchowski.com.toDoApp.model.Todo;
import kozuchowski.com.toDoApp.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ToDoServiceImpl implements ToDoService{

    private final ToDoRepository repository;

    @Autowired
    public ToDoServiceImpl(ToDoRepository repository) {
        this.repository = repository;
    }


    @Override
    public Todo create(CreateToDoDto dto) {
        Todo toDo = new Todo(dto.getTitle(), dto.getDeadline());
        return repository.save(toDo);
    }

    @Override
    public Todo find(Long id) {
        return null;
    }

    @Override
    public Todo update(CreateToDoDto dto) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
