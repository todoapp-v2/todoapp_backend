package kozuchowski.com.toDoApp.service;


import kozuchowski.com.toDoApp.dto.CreateUserDto;
import kozuchowski.com.toDoApp.exception.ObjectAlreadyExistException;
import kozuchowski.com.toDoApp.exception.PasswordMismatchException;
import kozuchowski.com.toDoApp.model.TodoUser;
import kozuchowski.com.toDoApp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public TodoUser create(CreateUserDto dto) throws ObjectAlreadyExistException, PasswordMismatchException {
        isEmailAvailable(dto.getEmail());
        doPassesMatch(dto.getPassword(), dto.getConfirmPass());
        TodoUser user = new TodoUser(dto.getName(), dto.getEmail(), dto.getPassword());
        return userRepository.save(user);
    }

    @Override
    public TodoUser read(Long id) {
        return null;
    }

    @Override
    public TodoUser update(CreateUserDto dto) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public boolean isEmailAvailable(String email) throws ObjectAlreadyExistException {
        Optional<TodoUser> optionalUser = userRepository.findByEmail(email);

        if(optionalUser.isPresent()) {
            throw new ObjectAlreadyExistException("Email is already in use");
        }
        return true;
    }

    @Override
    public boolean doPassesMatch(String pass, String confirm) throws PasswordMismatchException {
        if(!pass.equals(confirm)){
            throw new PasswordMismatchException("Password and confirm password do not match");
        }
        return true;
    }
}
