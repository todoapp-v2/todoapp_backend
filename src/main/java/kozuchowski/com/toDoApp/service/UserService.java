package kozuchowski.com.toDoApp.service;

import kozuchowski.com.toDoApp.dto.CreateUserDto;
import kozuchowski.com.toDoApp.exception.ObjectAlreadyExistException;
import kozuchowski.com.toDoApp.exception.PasswordMismatchException;
import kozuchowski.com.toDoApp.model.TodoUser;

public interface UserService {

    TodoUser create(CreateUserDto dto) throws ObjectAlreadyExistException, PasswordMismatchException;
    TodoUser read(Long id);
    TodoUser update(CreateUserDto dto);
    void delete (Long id);

    boolean isEmailAvailable(String email) throws ObjectAlreadyExistException;

    boolean doPassesMatch(String pass, String confirm) throws PasswordMismatchException;


}
