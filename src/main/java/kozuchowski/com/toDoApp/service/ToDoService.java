package kozuchowski.com.toDoApp.service;


import kozuchowski.com.toDoApp.dto.CreateToDoDto;
import kozuchowski.com.toDoApp.model.Todo;

public interface ToDoService {
    Todo create(CreateToDoDto dto);
    Todo find(Long id);
    Todo update(CreateToDoDto dto);
    void delete(Long id);

}
