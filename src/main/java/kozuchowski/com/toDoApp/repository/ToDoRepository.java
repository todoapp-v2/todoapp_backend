package kozuchowski.com.toDoApp.repository;

import kozuchowski.com.toDoApp.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoRepository extends JpaRepository<Todo, Long> {

}
