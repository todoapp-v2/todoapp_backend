package kozuchowski.com.toDoApp.repository;

import kozuchowski.com.toDoApp.model.TodoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<TodoUser, Long> {
    Optional<TodoUser> findByEmail(String email);
}
