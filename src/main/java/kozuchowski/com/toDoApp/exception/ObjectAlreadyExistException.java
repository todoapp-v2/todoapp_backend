package kozuchowski.com.toDoApp.exception;

public class ObjectAlreadyExistException extends Exception {

    public  ObjectAlreadyExistException() {};
    public ObjectAlreadyExistException(String message) {
        super(message);
    }

    public ObjectAlreadyExistException(Throwable cause) {
        super(cause);
    }

    public ObjectAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }
 }
