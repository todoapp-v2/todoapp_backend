package kozuchowski.com.toDoApp.exception;

public class PasswordMismatchException extends Exception{
    public  PasswordMismatchException() {};
    public PasswordMismatchException(String message) {
        super(message);
    }

    public PasswordMismatchException(Throwable cause) {
        super(cause);
    }

    public PasswordMismatchException(String message, Throwable cause) {
        super(message, cause);
    }
}
