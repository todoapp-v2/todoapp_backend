package kozuchowski.com.toDoApp.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class CreateToDoDto {

    @NotBlank(message = "Title is mandatory")
    private String title;
    @DateTimeFormat
    private LocalDate deadline;
}
