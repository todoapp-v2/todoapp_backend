package kozuchowski.com.toDoApp.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CreateUserDto {

    @NotBlank(message = "Name is mandatory")
    private String name;
    @Pattern(regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$", message = "Email is not valid")
    private String email;

    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!@#$%^&*()_+{}|;:',.<>?/])[A-Za-z\\d!@#$%^&*()_+{}|;:',.<>?/]{8,}$",
            message = "Password must be at least 8 characters long and contain at least one letter, one digit, and one special character from the following set: !@#$%^&*()_+{}|;:',.<>?/")
    private String password;
    @NotNull(message = "password cannot be null")
    private String confirmPass;
}
