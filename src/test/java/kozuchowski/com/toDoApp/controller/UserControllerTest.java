package kozuchowski.com.toDoApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kozuchowski.com.toDoApp.dto.CreateUserDto;
import kozuchowski.com.toDoApp.model.TodoUser;
import kozuchowski.com.toDoApp.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class UserControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    ObjectMapper objectMapper;

    private TodoUser user;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        objectMapper.findAndRegisterModules();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();

    }

    @AfterEach
    void afterEach() {
        if (user != null) {
            userRepository.deleteById(user.getId());
        }
    }


    @Test
    void shouldCreateUser() throws Exception {
        CreateUserDto createUserDto = new CreateUserDto("marek", "test@g.com", "Abc@1234", "Abc@1234");
        String requestBody = objectMapper.writeValueAsString(createUserDto);

        MvcResult result = this.mvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk())
                .andReturn();

        TodoUser user = objectMapper.readValue(result.getResponse().getContentAsString(), TodoUser.class);
        assertNotNull(user);
        assertEquals(createUserDto.getName(), user.getName());
        assertEquals(createUserDto.getEmail(), user.getEmail());

    }

    @Test
    void shouldThrowObjectAlreadyExistException() throws Exception {
        CreateUserDto createUserDto = new CreateUserDto("marek", "test@g.com", "Abc@1234", "Abc@1234");
        String requestBody = objectMapper.writeValueAsString(createUserDto);

            this.mvc.perform(post("/api/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
                    .andExpect(status().isBadRequest());
    }

    @Test
    void shouldThrowPasswordMismatchException() throws Exception {
        CreateUserDto createUserDto = new CreateUserDto("marek", "test@g.com", "Abc@1234", "Cba@1234");
        String requestBody = objectMapper.writeValueAsString(createUserDto);
        this.mvc.perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error").value("Password and confirm password do not match"));
    }

}